FROM alpine:latest as builder
MAINTAINER b3vis
ARG BORG_VERSION=1.1.10
ARG BORGMATIC_VERSION=1.2.15
RUN apk upgrade --no-cache \
    && apk add --no-cache \
    alpine-sdk \
    python3-dev \
    py3-pip \
    openssl-dev \
    lz4-dev \
    acl-dev \
    linux-headers \
    fuse-dev \
    attr-dev \
    && pip3 install --upgrade pip \
    && pip3 install --upgrade borgbackup==${BORG_VERSION} \
    && pip3 install --upgrade borgmatic==${BORGMATIC_VERSION} \
    && pip3 install llfuse

RUN curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip \
    && unzip rclone-current-linux-amd64.zip \
    && cd rclone-*-linux-amd64 \
    && cp rclone /usr/bin/ \
    && chmod 755 /usr/bin/rclone

FROM alpine:latest
MAINTAINER philippe.trepanier
COPY entry.sh /entry.sh
COPY backup_pg.sh /backup_pg.sh
RUN apk upgrade --no-cache \
    && apk add --no-cache \
    tzdata \
    sshfs \
    python3 \
    py3-pip \
    openssl \
    ca-certificates \
    lz4-libs \
    libacl \
    postgresql-client \
    && mkdir /config /cache /source /repository /root/.ssh \
    && rm -rf /var/cache/apk/* \
    && chmod 755 /entry.sh \
    && chmod 755 /backup_pg.sh
VOLUME /config
VOLUME /etc/borgmatic.d
VOLUME /cache
VOLUME /source
VOLUME /repository
VOLUME /root/.ssh
COPY --from=builder /usr/lib/python3.8/site-packages /usr/lib/python3.8/
COPY --from=builder /usr/bin/borg /usr/bin/
COPY --from=builder /usr/bin/borgfs /usr/bin/
COPY --from=builder /usr/bin/borgmatic /usr/bin/
COPY --from=builder /usr/bin/generate-borgmatic-config /usr/bin/
COPY --from=builder /usr/bin/upgrade-borgmatic-config /usr/bin/
COPY --from=builder /usr/bin/rclone /usr/bin/
# Set Envars
ENV BORG_CACHE_DIR /cache
CMD ["/entry.sh"]
