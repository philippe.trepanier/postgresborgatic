#!/bin/sh
# Setup repo
borg init --encryption=repokey /repository

# Export key
borg key export /repository/ /config/key

# Import your cron file
/usr/bin/crontab /config/crontab.txt
# Start cron
/usr/sbin/crond -f -L /dev/stdout
